﻿using System.Windows.Input;
using Xamarin.Forms;

namespace MonkeyHubApp.Controls
{
    public class MyListView : ListView
    {
        public static readonly BindableProperty ItemTappedCommandProperty =
                        BindableProperty.Create("ItemTappedCommand", typeof(ICommand), typeof(ListView), null);

        public ICommand ItemTappedCommand
        {
            get { return (ICommand)GetValue(ItemTappedCommandProperty); }
            set { SetValue(ItemTappedCommandProperty, value); }
        }

        bool _initialized;

        public MyListView(ListViewCachingStrategy strategy) : base(strategy)
        {
            Initialize();
        }

        public MyListView() : this(ListViewCachingStrategy.RecycleElement)
        {
            Initialize();
        }

        private void Initialize()
        {
            if (!_initialized)
            {
                ItemSelected += (sender, e) =>
                    {
                        if (ItemTappedCommand == null)
                        {
                            return;
                        }

                        if (ItemTappedCommand.CanExecute(e.SelectedItem))
                            ItemTappedCommand.Execute(e.SelectedItem);
                    }; 
            }

            _initialized = true;
        }
    }
}
