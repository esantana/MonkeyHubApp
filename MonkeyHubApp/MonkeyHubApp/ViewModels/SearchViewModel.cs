﻿using MonkeyHubApp.Models;
using MonkeyHubApp.Services;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace MonkeyHubApp.ViewModels
{
    public class SearchViewModel : BaseViewModel
    {
        private readonly IMonkeyHubApiService _monkeyHubApiService;
        private string _searchTerm;
        public string SearchTerm
        {
            get { return _searchTerm; }
            set
            {
                if (SetProperty(ref _searchTerm, value))
                    SearchCommand.ChangeCanExecute();
            }
        }

        public Command SearchCommand { get; }
        public ObservableCollection<Content> SearchResults { get; }
        public Command ShowContentCommand { get; }

        public SearchViewModel(IMonkeyHubApiService monkeyHubApiService)
        {
            _monkeyHubApiService = monkeyHubApiService;

            SearchResults = new ObservableCollection<Content>();
            SearchCommand = new Command(ExecuteSearchCommand, CanExecuteSearchCommand);
            ShowContentCommand = new Command<Content>(ExecuteShowContentCommand);
        }

        bool CanExecuteSearchCommand()
        {
            return !string.IsNullOrWhiteSpace(SearchTerm);
        }

        async void ExecuteSearchCommand()
        {
            var searchResults = await _monkeyHubApiService.GetContentsByFilterAsync(SearchTerm);
            SearchResults.Clear();

            if (searchResults == null)
            {
                await DisplayAlert("MonkeyHub", "Nenhum resultado encontrado.", "OK");
                return;
            }

            foreach (var content in searchResults)
            {
                SearchResults.Add(content);
            }
        }

        async void ExecuteShowContentCommand(Content content)
        {
            await PushAsync<ContentWebViewModel>(content);
        }
    }
}
