﻿using MonkeyHubApp.Services;
using MonkeyHubApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MonkeyHubApp
{
    public partial class MainPage : ContentPage
    {
        private MainViewModel ViewModel => BindingContext as MainViewModel;

        public MainPage()
        {
            InitializeComponent();
            var monkeyHubApiService = DependencyService.Get<IMonkeyHubApiService>();
            BindingContext = new MainViewModel(monkeyHubApiService);
        }
        
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            //if (ViewModel != null)
            //{
                await ViewModel?.LoadAsync();
            //}
        }
    }
}
