﻿using MonkeyHubApp.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MonkeyHubApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPage : ContentPage
    {
        private SearchViewModel ViewModel => BindingContext as SearchViewModel;

        public SearchPage()
        {
            InitializeComponent();
        }
    }
}
